<?php

namespace Drupal\geolocation_leafletcenternode\Plugin\geolocation\MapFeature;

use Drupal\geolocation\MapFeatureBase;
use Drupal\geolocation\MapProviderInterface;

/**
 * Provides Leaflet Center Node Feature.
 *
 * @MapFeature(
 *   id = "leaflet_center_node",
 *   name = @Translation("Center Node"),
 *   description = @Translation("Center on current node location. Works in views only!"),
 *   type = "leaflet",
 * )
 */
class LeafletCenterNode extends MapFeatureBase {

  /**
   * {@inheritdoc}
   */
  public static function getDefaultSettings(): array {
    return array_replace_recursive(
      parent::getDefaultSettings(),
      [
        'min_zoom' => 0,
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm(array $settings, array $parents = [], ?MapProviderInterface $mapProvider = NULL): array {
    $form = parent::getSettingsForm($settings, $parents, $mapProvider);
    $form['min_zoom'] = [
      '#type' => 'number',
      '#min' => 0,
      '#step' => 1,
      '#title' => $this->t('Zoom level'),
      '#default_value' => $settings['min_zoom'],
    ];
    return $form;
  }

}
