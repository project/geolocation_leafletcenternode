CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

The Geolocation Leaflet Center Node module allows to hide fields, if the cookie
category is not accepted.


 * For a full description of the module visit:
   https://www.drupal.org/project/geolocation_leafletcenternode

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/geolocation_leafletcenternode


REQUIREMENTS
------------

This module requires the Geolocation module.


INSTALLATION
------------

Install the Geolocation Leaflet Center Node module as you would normally
install a contributed Drupal module. Visit https://www.drupal.org/node/1897420
for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the Geolocation Leaflet
       Center Node module.
    2. Change center settings in view settings.


TROUBLESHOOTING
---------------


FAQ
---


MAINTAINERS
-----------

 * sleitner - https://www.drupal.org/u/sleitner
