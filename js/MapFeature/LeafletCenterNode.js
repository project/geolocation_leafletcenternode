import { LeafletMapFeature } from '../../../../contrib/geolocation/modules/geolocation_leaflet/js/MapFeature/LeafletMapFeature.js';

export default class LeafletCenterNode extends LeafletMapFeature {
  onMarkerAdded(marker) {
    super.onMarkerAdded(marker);
    if (
      marker.settings.wrapper.dataset.entityType &&
      drupalSettings.path.currentPath ===
        marker.settings.wrapper.dataset.entityType.concat(
          '/',
          marker.settings.wrapper.dataset.entityId,
        )
    ) {
      this.map.setCenterByCoordinates(marker.coordinates);
      if (this.settings.min_zoom !== '') {
        this.map.setZoom(this.settings.min_zoom);
      }
    }
  }
}
